const initialState = {
  products: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case "SAVE_ITEM":
      return {
        products: [action.payload, ...state.products]
      };
    case "DELETE_ITEM":
      return {
        products: state.products.filter(el => el.id !== action.payload)
      };
    case "EDIT_PRODUCT":
      return {
        products: state.products.map((el, i) => {
          if (el.id !== action.payload.id) {
            return el;
          } else {
            return action.payload;
          }
        })
      };
    default:
      return { ...state };
  }
}
